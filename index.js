/*
 * @author 	: Yash Karanke
 * @description : Command Line YouTube Downloader
 * @usage	: node index.js [{YouTubeID}] [fileName.mp4]
 * @example	: node index.js tp37V5B4LEM Samurai.mp4
 */
const fs 	= require("fs");
const ytdl 	= require("ytdl-core");
const parentURL = "https://www.youtube.com/watch?v=";
const argument	= process.argv[2];
const fileName	= process.argv[3];

	ytdl(parentURL+argument)
		.pipe(fs.createWriteStream(fileName));
